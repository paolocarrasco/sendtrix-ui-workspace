#!/usr/bin/env bash

sudo apt-get --yes update

# Essentials
sudo apt-get install --yes build-essential
sudo apt-get install --yes curl
sudo apt-get install --yes wget

# Ruby
sudo add-apt-repository --yes ppa:brightbox/ruby-ng
sudo apt-get --yes update
sudo apt-get install --yes ruby2.2
sudo apt-get install --yes ruby2.2-dev
sleep 10 # A short time is necessary for the Linux file system to recognize gem

# NodeJs
curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash -
sudo apt-get install --yes nodejs

# development tools
sleep 10 # A short time is necessary for the Linux file system to recognize npm
sudo apt-get install --yes git
sudo gem install bundler --no-ri --no-rdoc
sudo npm install --global bower
sudo npm install --global grunt-cli
